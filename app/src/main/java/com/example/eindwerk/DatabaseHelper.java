package com.example.eindwerk;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.time.LocalDateTime;

public class DatabaseHelper extends SQLiteOpenHelper  {

    public static final String DATABASE_FRIET = "friet.db";


    public static final String TABLE_ALLERGENEN = "allergenen";
    public static final String COL_1_al = "ALLERGENEN_ID";
    public static final String COL_2_al = "ALLERGEEN";

    public static final String TABLE_PRODUCTEN = "producten";
    public static final String COL_1_pr = "PRODUCT_ID";
    public static final String COL_2_pr = "PRODUCT_NAME";
    public static final String COL_3_pr = "PRODUCT_INFO";
    public static final String COL_4_pr = "PRODUCT_QUANTITY";
    public static final String COL_5_pr = COL_1_al;
    public static final String COL_6_pr = "PRICE";
    public static final String COL_7_pr = "CATEGORY";


    public static final String TABLE_BESTELLING = "bestelling";
    public static final String COL_1 = "BESTELLING_ID";
    public static final String COL_2 = COL_1_pr;
    public static final String COL_3 = "BESTELLING_DATE";
    public static final String COL_4 = "TOTAALPRIJS";


    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_FRIET, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_ALLERGEEN = "CREATE TABLE " + TABLE_ALLERGENEN + "(" + COL_1_al + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_2_al + " STRING );";
        db.execSQL(CREATE_TABLE_ALLERGEEN);

        String CREATE_TABLE_PRODUCTEN = "CREATE TABLE " + TABLE_PRODUCTEN + "(" + COL_1_pr + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_2_pr + " STRING, " + COL_3_pr + " STRING , " + COL_4_pr + " INTEGER, " + COL_5_pr + " INTEGER, " + COL_7_pr + " STRING, " + COL_6_pr + " DOUBLE,FOREIGN KEY(ALLERGENEN_ID) REFERENCES "+ TABLE_ALLERGENEN+"(COL_5_pr));";
        db.execSQL(CREATE_TABLE_PRODUCTEN);


        String CREATE_TABLE_BESTELLINGEN = "CREATE TABLE " + TABLE_BESTELLING + "(" + COL_1 + " INTEGER PRIMARY KEY, " + COL_2 + " STRING NOT NULL, " + COL_3 + " DATETIME, " + COL_4 + " FLOAT );";
        db.execSQL(CREATE_TABLE_BESTELLINGEN);

        ContentValues allergeen = new ContentValues();
        allergeen.put(COL_1_al, 1);
        allergeen.put(COL_2_al, "NUTS");
        long resAllergeen = db.insert(TABLE_ALLERGENEN,null,allergeen);






    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BESTELLING);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ALLERGENEN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTEN);
        onCreate(db);

    }

    public void addProduct(String name, String info, int quantity, int alId, double price, String category){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2_pr, name);
        contentValues.put(COL_3_pr, info);
        contentValues.put(COL_4_pr, quantity);
        contentValues.put(COL_5_pr, 1);
        contentValues.put(COL_6_pr, price);
        contentValues.put(COL_7_pr, category);
        long result = db.insert(TABLE_PRODUCTEN,null,contentValues);


    }



    public boolean addDataBestelling(int id, LocalDateTime dateTime, float prijs){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2,id);
        contentValues.put(COL_3, String.valueOf(dateTime));
        contentValues.put(COL_4,prijs);

        long result = db.insert(TABLE_BESTELLING,null,contentValues);
//        ContentValues allergeen = new ContentValues();
//        allergeen.put(COL_1_al, 50);
//        allergeen.put(COL_2_al, "APPEL");
//
//        long resAllergeen = db.insert(TABLE_ALLERGENEN,null,allergeen);




        if (result == -1 ){
            return false;
        }else {
            return true;
        }
    }
    public void deleteOrderItem(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_BESTELLING, COL_1 + " = ?", new String[] { String.valueOf(id) });
        db.close();
    }
}
