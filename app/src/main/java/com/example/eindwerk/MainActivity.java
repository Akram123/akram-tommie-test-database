package com.example.eindwerk;

import android.content.Context;
import android.os.Bundle;

import com.example.eindwerk.Utils.JSONparser;
import com.example.eindwerk.Utils.StringParser;


import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDate;
import java.time.LocalDateTime;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    DatabaseHelper frietDB;
    Button btnAddFriet;
    Button btnAddSnack;
    Button btnAddBurger;
    StringParser stringParser;
    JSONparser jsoNparser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        frietDB = new DatabaseHelper(this);
        stringParser = new StringParser();
        jsoNparser = new JSONparser();

        btnAddFriet = (Button) findViewById(R.id.frietbutton);
        btnAddSnack = (Button) findViewById(R.id.snackButton);
        btnAddBurger = (Button) findViewById(R.id.burgerButton);
        addDataBestteling();
        loadData();



    }

    private void loadData() {
        Context c = MainActivity.this;

        JSONArray jsonarray = null;
        try {
            jsonarray = new JSONArray(jsoNparser.readJSON(c));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < jsonarray.length(); i++) {
            try {
                JSONObject jsonobject = jsonarray.getJSONObject(i);
                String name = jsonobject.getString("naam");
                double prijs = jsonobject.getDouble("prijs");
                String category = jsonobject.getString("category");
                String info = jsonobject.getString("info");
                int quantity = jsonobject.getInt("quantity");
                System.out.println(i+ ": " + name + " " + prijs +" " + category + " " + info + " " + quantity);
                frietDB.addProduct(name, info, quantity, 1, prijs, category );


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public void addDataBestteling(){
        btnAddFriet.setOnClickListener(new View.OnClickListener(){
            
            // Test fase
            @Override
            public void onClick(View v) {
                LocalDateTime dateTimeVAR = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    dateTimeVAR = LocalDate.of(2020,2,2).atStartOfDay();
                }

                int productId = 1;
                LocalDateTime dateTime = dateTimeVAR;
                float prijs = (float) 20.0;

                boolean insertData = frietDB.addDataBestelling(productId,dateTime,prijs);
                if (insertData == true){
                    Toast.makeText(MainActivity.this, "Friet Data gelukt!!!", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MainActivity.this, "Mission failed", Toast.LENGTH_SHORT).show();
                }
            }

        });
        btnAddSnack.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                String ints = "1, 2, 3";
                for (Integer i: stringParser.StringToList(ints)){
                    Toast.makeText(MainActivity.this, i.toString(), Toast.LENGTH_SHORT).show();
                }
                ints = "1, 2, 3, 4, 452";
                List<Integer> ll = stringParser.StringToList(ints);



            }

        });
        btnAddBurger.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                float a = (float) 2.5;
                frietDB.addProduct("curryworst", "info", 0, 1, a, "snacks" );
            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}