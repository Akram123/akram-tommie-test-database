package com.example.eindwerk.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringParser {
    public List<Integer> StringToList(String ints){
        List<String> strlist = new ArrayList<String>(Arrays.asList(ints.split(", ")));
        List<Integer> intlist = new ArrayList<Integer>();
        for(String s : strlist){
            intlist.add(Integer.valueOf(s));
        }
        return intlist;
    }
    public String ListToString(List<Integer> lst){
        return Arrays.toString(lst.toArray()).replace("[", "").replace("]", "");
    }
}
