package com.example.eindwerk.Utils;


import android.content.Context;

import com.example.eindwerk.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class JSONparser {
    MainActivity main;



    public String readJSON(Context c) {
        String json = null;
        try {

            InputStream is = c.getAssets().open("producten.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


}
